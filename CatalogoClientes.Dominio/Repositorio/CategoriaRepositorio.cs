﻿using CatalogoClientes.Dominio.Entidades;
using System.Collections.Generic;
using System.Linq;

namespace CatalogoClientes.Dominio.Repositorio
{
  public class CategoriaRepositorio : IRepositorio<Categoria>
  {
    private NorthwindContexto contexto;

    public CategoriaRepositorio(NorthwindContexto northwindContexto)
    {
      this.contexto = northwindContexto;
    }

    public IEnumerable<Categoria> GetTodos()
    {
      return contexto.Categorias.OrderBy(x => x.CategoryName).ToList();
    }
  }
}

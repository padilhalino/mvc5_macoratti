﻿using CatalogoClientes.Dominio.Entidades;
using System.Collections.Generic;
using System.Linq;

namespace CatalogoClientes.Dominio.Repositorio
{
    public class ClienteRepositorio : IRepositorio<Cliente>
    {
        private NorthwindContexto contexto;

        public ClienteRepositorio(NorthwindContexto northwindContexto)
        {
            this.contexto = northwindContexto;
        }

        public IEnumerable<Cliente> GetTodos()
        {
            return contexto.Clientes.OrderBy(x => x.Nome).ToList();
        }
    }
}

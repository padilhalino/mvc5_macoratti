﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CatalogoClientes.Dominio.Entidades
{
  [Table("Categories")]
  public class Categoria
  {
    [Key]
    public int CategoryID { get; set; }
    [Required(ErrorMessage = "Informe o nome da Categoria")]
    public string CategoryName { get; set; }
    public string Description { get; set; }
    public byte[] Picture { get; set; }
  }
}

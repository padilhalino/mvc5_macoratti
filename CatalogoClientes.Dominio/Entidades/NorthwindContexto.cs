﻿using System.Data.Entity;

namespace CatalogoClientes.Dominio.Entidades
{
  public class NorthwindContexto : DbContext
  {
    public NorthwindContexto()
      : base("name=ConexaoClientes")
    { }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      Database.SetInitializer<NorthwindContexto>(new CreateDatabaseIfNotExists<NorthwindContexto>());
    }

    public DbSet<Cliente> Clientes { get; set; }
    public DbSet<Categoria> Categorias { get; set; }
  }
}

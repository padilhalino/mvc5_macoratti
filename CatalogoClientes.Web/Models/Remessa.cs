﻿using System.Collections.Generic;
using System.Web;

namespace CatalogoClientes.Web.Models
{
  public class Remessa
  {
    public IEnumerable<HttpPostedFileBase> Arquivos { get; set; }
  }
}
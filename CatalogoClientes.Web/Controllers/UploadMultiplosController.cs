﻿using CatalogoClientes.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CatalogoClientes.Web.Controllers
{
  /// <summary>
  /// No web.config, em <system.web><httpRuntime>:
  /// - maxRequestLength: default 4096 kb. Request máximo.
  /// - requestLengthDiskThreshold: default 80 kb.  Buffer de memória máximo.
  /// - executionTimeout: default 110 segundos. Tempo de execução máximo.
  /// </summary>
  public class UploadMultiplosController : Controller
  {
    // GET: Remessa
    public ActionResult Index()
    {
      return View();
    }

    // GET: Remessa
    [HttpPost]
    public ActionResult Index(Remessa arq)
    {
      try
      {
        string nomeArquivo = "";
        string arquivosEnviados = "";
        foreach (var arquivo in arq.Arquivos)
        {
          if (arquivo.ContentLength > 0)
          {
            nomeArquivo = Path.GetFileName(arquivo.FileName);
            var caminho = Path.Combine(Server.MapPath("~/Imagens/Uploaded"), nomeArquivo);
            arquivo.SaveAs(caminho);
          }

          arquivosEnviados += ", " + nomeArquivo;
        }
        ViewBag.Mensagem = "Arquivo(s): " + arquivosEnviados + ", enviado(s) com sucesso.";
      }
      catch (Exception ex)
      {
        ViewBag.Mensagem = "Erro: " + ex.Message;
      }

      return View();
    }
  }
}
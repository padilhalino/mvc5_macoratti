﻿using CatalogoClientes.Dominio.Entidades;
using CatalogoClientes.Dominio.Repositorio;
using System.Web.Mvc;

namespace CatalogoClientes.Web.Controllers
{
  public class CategoriaController : Controller
  {
    private NorthwindContexto db = new NorthwindContexto();
    private IRepositorio<Categoria> _repositorioCategoria;

    public CategoriaController()
    {
      _repositorioCategoria = new CategoriaRepositorio(new NorthwindContexto());
    }

    // GET: Categoria
    public ActionResult Categoria()
    {
      ViewBag.CategoriaId = new SelectList(db.Categorias, "CategoryID", "CategoryName");
      //ViewBag.CategoriaId = new SelectList(db.Categorias, "CategoryID", "CategoryName", "3");
      return View();
    }

    // POST: Categoria
    [HttpPost]
    public ActionResult Categoria(string CategoriaId)
    {
      string valor = CategoriaId;
      string valorRequest = Request["CategoriaId"];

      // Recarrega pra não dar erro
      ViewBag.CategoriaId = new SelectList(db.Categorias, "CategoryID", "CategoryName");
      return View();
    }
  }
}
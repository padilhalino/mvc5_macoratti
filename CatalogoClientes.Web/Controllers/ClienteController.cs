﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CatalogoClientes.Dominio.Entidades;
using CatalogoClientes.Dominio.Repositorio;
using PagedList;

namespace CatalogoClientes.Web.Controllers
{
  public class ClienteController : Controller
  {
    private NorthwindContexto db = new NorthwindContexto();
    private IRepositorio<Cliente> _repositorioCliente;

    // GET: Cliente
    public ClienteController()
    {
      _repositorioCliente = new ClienteRepositorio(new NorthwindContexto());
    }

    public ActionResult Catalogo(int? pagina)
    {
      int tamanhoPagina = 1;
      int numeroPagina = pagina ?? 1;
      return View(_repositorioCliente.GetTodos().ToPagedList(numeroPagina, tamanhoPagina));
    }

    //// GET: Cliente
    //public ActionResult Index()
    //{
    //  return View(db.Clientes.ToList());
    //}

    // GET: Cliente/Details/5
    public ActionResult Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      Cliente cliente = db.Clientes.Find(id);
      if (cliente == null)
      {
        return HttpNotFound();
      }
      return View(cliente);
    }

    // GET: Cliente/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: Cliente/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "ClienteId,Nome,Email,Endereco,Imagem,ImagemTipo")] Cliente cliente, HttpPostedFileBase uploadImg)
    {
      if (ModelState.IsValid)
      {
        if (uploadImg != null && uploadImg.ContentLength > 0)
        {
          var arqImagem = new Cliente
          {
            ImagemTipo = uploadImg.ContentType
          };
          using (var reader = new BinaryReader(uploadImg.InputStream))
          {
            arqImagem.Imagem = reader.ReadBytes(uploadImg.ContentLength);
          }
          cliente.Imagem = arqImagem.Imagem;
          cliente.ImagemTipo = arqImagem.ImagemTipo;
        }

        db.Clientes.Add(cliente);
        db.SaveChanges();
        TempData["mensagem"] = string.Format("{0} : foi incluído com sucesso", cliente.Nome);
        return RedirectToAction("Catalogo");
      }

      return View(cliente);

      //if (ModelState.IsValid)
      //{
      //  db.Clientes.Add(cliente);
      //  db.SaveChanges();
      //  return RedirectToAction("Catalogo");
      //}

      //return View(cliente);
    }

    // GET: Cliente/Edit/5
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      Cliente cliente = db.Clientes.Find(id);
      if (cliente == null)
      {
        return HttpNotFound();
      }
      return View(cliente);
    }

    // POST: Cliente/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit([Bind(Include = "ClienteId,Nome,Email,Endereco,Imagem,ImagemTipo")] Cliente cliente, HttpPostedFileBase uploadImg)
    {
      if (ModelState.IsValid)
      {
        if (uploadImg != null && uploadImg.ContentLength > 0)
        {
          var arqImagem = new Cliente
          {
            ImagemTipo = uploadImg.ContentType
          };
          using (var reader = new BinaryReader(uploadImg.InputStream))
          {
            arqImagem.Imagem = reader.ReadBytes(uploadImg.ContentLength);
          }
          cliente.Imagem = arqImagem.Imagem;
          cliente.ImagemTipo = arqImagem.ImagemTipo;
        }

        db.Entry(cliente).State = EntityState.Modified;
        db.SaveChanges();
        TempData["mensagem"] = string.Format("{0} : foi atualizado com sucesso", cliente.Nome);
        return RedirectToAction("Catalogo");
      }

      return View(cliente);

      //if (ModelState.IsValid)
      //{
      //  db.Entry(cliente).State = EntityState.Modified;
      //  db.SaveChanges();
      //  return RedirectToAction("Catalogo");
      //}
      //return View(cliente);
    }

    // GET: Cliente/Delete/5
    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      Cliente cliente = db.Clientes.Find(id);
      if (cliente == null)
      {
        return HttpNotFound();
      }
      return View(cliente);
    }

    // POST: Cliente/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      Cliente cliente = db.Clientes.Find(id);
      db.Clientes.Remove(cliente);
      db.SaveChanges();
      TempData["mensagem"] = string.Format("{0} : foi excludo com sucesso", cliente.Nome);
      return RedirectToAction("Catalogo");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
